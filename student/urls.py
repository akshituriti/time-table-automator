from django.urls import path

from . import views

urlpatterns = [
    path("", views.student_home, name="student-home"),
    path("change-password", views.change_password, name="change-password")
]