from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect

# Create your views here.
def login(request):
    return render(request, "login/login.html")

def forgot_password(request):
    return render(request, "login/forgot-password.html")